//
//  CourseCell.swift
//  LearnIT
//
//  Created by iosdev on 24/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class CourseCell: UITableViewCell {
    @IBOutlet weak var courseLabel: UILabel!
    @IBOutlet weak var courseCollection: UICollectionView!
}
