//
//  ViewController2.swift
//  LangMaster
//
//  Created by Thath on 20/04/2019.
//  Copyright © 2019 Thath. All rights reserved.
//

import UIKit
import CoreData

// create this class to control the BookmarkView
class BookmarkViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITabBarControllerDelegate {
    
    @IBOutlet var bookmarkTableView: UITableView!
    var selectedCourse:Courses!       // this variable will keep the course's info selected
    
    override func viewDidLoad() {
        
        self.title = NSLocalizedString("Bookmarks", comment: "Title of view")      // set title for BookmarkView
        editButton.title = NSLocalizedString("Edit", comment: "Edit the Bookmarks")      // set title for edit button
        
        // Adds gesture recognizer for tab swiping
        if bookmarkTableView.isEditing {
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped(_:)))
            swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
            swipeLeft.delegate = self
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped(_:)))
            swipeRight.direction = UISwipeGestureRecognizer.Direction.right
            swipeRight.delegate = self
            self.view.addGestureRecognizer(swipeLeft)
            self.view.addGestureRecognizer(swipeRight)
        }
    }
    
    // Swiping between tabs
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .left {
            if (self.tabBarController?.selectedIndex)! < 2
            { // set here  your total tabs
                self.tabBarController?.selectedIndex += 1
            }
        } else if gesture.direction == .right {
            if (self.tabBarController?.selectedIndex)! > 0 {
                self.tabBarController?.selectedIndex -= 1
            }
        }
    }
    
    // set up the number of row in the bookmark tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookmarkCourses.count
    }
    
    // set up the cell with course's info and design
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookmarkCell", for: indexPath) as! BookmarkTableViewCell
        let url = NSURL(string: bookmarkCourses[indexPath.item].linkToImage)
        if let data = NSData(contentsOf: url! as URL) {
            cell.courseImageView.contentMode = UIView.ContentMode.scaleAspectFit
            cell.courseImageView.image = UIImage(data: data as Data)
            cell.courseTitleLabel.text = bookmarkCourses[indexPath.item].title
            cell.courseDescriptionLabel.text = bookmarkCourses[indexPath.item].description
            cell.courseAuthorLabel.text = bookmarkCourses[indexPath.item].author + " | " + bookmarkCourses[indexPath.item].updateTime
            cell.courseView.backgroundColor = cellBackgroundColor(cellIndex: indexPath.row, numberOfElements: bookmarkCourses.count)
            cell.selectionStyle = .none
            cell.courseView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            cell.courseView.layer.borderWidth = 1
            cell.courseView.layer.cornerRadius = 12
        }
        return cell
    }
    
    // editting bookmark courses feature is only availabel if bookmark button clicked
    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    {
        if !bookmarkTableView.isEditing {
            return UITableViewCell.EditingStyle.none
        } else {
            return UITableViewCell.EditingStyle.delete
        }
    }
    
    // allow to move rows
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // this function handles reordering rows
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let idItem = bookmarkCourseIdArray[sourceIndexPath.row]
        let courseItem = bookmarkCourses[sourceIndexPath.row]
        bookmarkCourseIdArray.remove(at: sourceIndexPath.row)
        bookmarkCourses.remove(at: sourceIndexPath.row)
        bookmarkCourseIdArray.insert(idItem, at: destinationIndexPath.row)
        bookmarkCourses.insert(courseItem, at: destinationIndexPath.row)
        
        let fetchRequest: NSFetchRequest<BookmarkCourseArray> = BookmarkCourseArray.fetchRequest()
        if let result = try? PersistenceService.context.fetch(fetchRequest) {
            for object in result as [NSManagedObject] {
                    PersistenceService.context.delete(object);
            }
            for object in bookmarkCourseIdArray {
                BookmarkCourseArray(context: PersistenceService.context).bookmarkCourseId = Int16(object)
                PersistenceService.saveContext();                 // update the change core date
            }
        }
        bookmarkTableView.reloadData()                    // reload data to update UI
    }
    
    // this function will be called to lead the user to the website of course when a row selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCourse = bookmarkCourses[indexPath.row]
        performSegue(withIdentifier: "bookmarkToWeb", sender: self)
    }
    
    // create a segue to pass the course's info and navigate to WebPage View
    override func prepare(for segue: UIStoryboardSegue, sender: (Any)?) {
        let svc = segue.destination as? WebPageViewController
        svc?.courseBrief = selectedCourse
    }

    // delete the cell
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let fetchRequest: NSFetchRequest<BookmarkCourseArray> = BookmarkCourseArray.fetchRequest()
            if let result = try? PersistenceService.context.fetch(fetchRequest) {
                for object in result as [NSManagedObject] {
                    if object.value(forKey: "bookmarkCourseId") as! Int == bookmarkCourseIdArray[indexPath.row]{
                     PersistenceService.context.delete(object);
                     }
                }
            }
            bookmarkCourseIdArray.remove(at: indexPath.row)   // remove the course id from the bookmark course id array
            bookmarkCourses.remove(at: indexPath.row)         // remove the course from the bookmark course array
            PersistenceService.saveContext();                 // update the change core date
            bookmarkTableView.reloadData()                    // reload data to update UI
        }
    }
    
    @IBOutlet var editButton: UIBarButtonItem!
    @IBAction func editBookmarks(_ sender: Any) {
        //change the text and color on the edit button
        bookmarkTableView.isEditing = !bookmarkTableView.isEditing
        switch bookmarkTableView.isEditing {
        case true:

            editButton.tintColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            editButton.title = NSLocalizedString("Done", comment: "Editing bookmarks is done")
        default:
            editButton.tintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
            editButton.title = NSLocalizedString("Edit", comment: "Edit the bookmarks")
        }
    }
    
    var bookmarkCourseIdArray = [Int]()      // this variable will contain bookmark course ids
    var bookmarkCourses: [Courses] = []     // this variable will contain bookmark courses
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bookmarkTableView.separatorStyle = .none
    }
    
    // small algorithm to set the cell background color automatically from important to less important item
    func cellBackgroundColor( cellIndex: Int, numberOfElements: Int) -> UIColor {
        self.tabBarController?.tabBar.items?[1].badgeValue = String(self.bookmarkCourses.count)
        switch Double(cellIndex)/Double(numberOfElements) {
        case 0..<1/3:
            return #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
        case (1/3)..<(2/3):
            return #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        default:
            return #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // remove the breakline between cells
        self.bookmarkTableView.separatorStyle = .none
        
        // on navigation bar, set text color to white
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        // get courses data from Core Data
        self.bookmarkCourseIdArray = FetchCoreData().fetchbookMarkId();
        DispatchQueue.global(qos: .userInteractive).async {
            let dataFetcher = DataFetcher()
            dataFetcher.getJSONData(completionHandler: {jsonData, error in
                if let jsonData = jsonData {
                    self.bookmarkCourses = [] // clear this array before fetch the new data
                    // using a filter to find out bookmarks courses thank for course ids
                    for courseId in self.bookmarkCourseIdArray {
                        self.bookmarkCourses += (jsonData.courseLevels.basicCourses + jsonData.courseLevels.advancedCourses + jsonData.courseLevels.frameworkCourses).filter{$0.id == courseId}
                    }
                    // reload data to update UI
                    DispatchQueue.main.async {
                        self.bookmarkTableView.reloadData()
                    }
                }
            })
        }
    }
    
    // pop the current view to root View before going to another view on another tabbar
    override func viewDidDisappear(_ animated: Bool) {
        if bookmarkTableView.isEditing {
            self.bookmarkTableView.isEditing = false
        }
    }
}
