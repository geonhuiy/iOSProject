//
//  TransitionObject.swift
//  LearnIT
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

//Transition for switching between view tabs
class TransitionObject: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toView: UIView = (transitionContext.view(forKey: UITransitionContextViewKey.to))!
        let fromView: UIView = (transitionContext.view(forKey: UITransitionContextViewKey.from))!
        
        UIView.transition(from: fromView, to: toView, duration: transitionDuration(using: transitionContext), options: [.transitionCrossDissolve]) {
            finished in transitionContext.completeTransition(true)
        }
    }
    
    
}
