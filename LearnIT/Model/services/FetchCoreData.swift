//
//  FetchCoreData.swift
//  LearnIT
//
//  Created by Emil on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import CoreData

class FetchCoreData{
    
    func fetchbookMarkId() -> Array<Int>{
        var bookmarkCourseIdArray = [Int]()
        let fetchRequest: NSFetchRequest<BookmarkCourseArray> = BookmarkCourseArray.fetchRequest()
        //core data fetches the ids of the bookmarked courses
        do{
            let ids = try PersistenceService.context.fetch(fetchRequest)
            //takes all the unique values. (can be more short maybe!)
            for data in ids as [NSManagedObject] {
                if(!bookmarkCourseIdArray.contains(data.value(forKey: "bookmarkCourseId") as! Int)){
                    bookmarkCourseIdArray.append(data.value(forKey: "bookmarkCourseId") as! Int)
                }
            }
        } catch {
            fatalError();
        }
        return bookmarkCourseIdArray
    }
}

