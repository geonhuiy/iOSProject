//
//  DataFetcher.swift
//  LearnIT
//
//  Created by iosdev on 02/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

// this class is for loading json data from server, using completionHandler to make sure the function will return data.
class DataFetcher {
    var db: Database?
    func getJSONData(completionHandler: @escaping (Database?, Error?) -> Void){
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil
        let session = URLSession.init(configuration: config)
        guard let jsonDataURL = URL(string: "http://bit.ly/2XDl5T8") else {
            print("Wrong url")
            return
        }
        session.dataTask(with: jsonDataURL) {(data, response, error) in
        do {
            // Decodes data into defined struct structure
            let database = try JSONDecoder().decode(Database.self ,from: data!)
            // Completion handler for datatask, prevents nil data
            completionHandler(database, nil)
        }
        catch {
            completionHandler(nil, error)
        }
        }.resume()
    }
}
