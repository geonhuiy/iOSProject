//
//  BookmarkCourseArray+CoreDataProperties.swift
//  LearnIT
//
//  Created by iosdev on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension BookmarkCourseArray {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookmarkCourseArray> {
        return NSFetchRequest<BookmarkCourseArray>(entityName: "BookmarkCourseArray")
    }

    @NSManaged public var bookmarkCourseId: Int16

}
