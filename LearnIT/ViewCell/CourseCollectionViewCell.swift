//
//  CourseCollectionViewCell.swift
//  LearnIT
//
//  Created by Thath on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

// create a class for CourseCollectionView cell
class CourseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLable: UILabel!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var priceLable: UILabel!
}
