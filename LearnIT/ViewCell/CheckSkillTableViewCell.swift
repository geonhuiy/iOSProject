//
//  CheckSkillTableViewCell.swift
//  LangMaster
//
//  Created by Thath on 02/05/2019.
//  Copyright © 2019 Thath. All rights reserved.
//

import UIKit

// create a prototype ProfileCellSubclassDelegate with 2 function searchButtonTapped and checkBoxTapped that have a parameter as the current cell, this helps us to access to the current cell index.
protocol ProfileCellSubclassDelegate: class {
    func searchButtonTapped(cell: CheckSkillTableViewCell, sender: UIButton)
    func checkBoxTapped(cell: CheckSkillTableViewCell, sender: UIButton)
}

// create a class for CheckSkillTableView cell
class CheckSkillTableViewCell: UITableViewCell {
    @IBOutlet var languageLevelLabel: UILabel!
    @IBOutlet var checkBoxLabel: UIButton!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var courseView: UIView!
    
    weak var delegate: ProfileCellSubclassDelegate?
    
    // the searchButtonClicked will pass current cell to searchButtonTapped function
    @IBAction func searchButtonClicked(_ sender: Any) {
        self.delegate?.searchButtonTapped(cell: self, sender: sender as! UIButton)
    }
    
    // the checkBoxClicked will pass current cell to checkBoxTapped function
    @IBAction func checkBoxClicked(_ sender: Any) {
        self.delegate?.checkBoxTapped(cell: self, sender: sender as! UIButton)
    }
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        self.delegate = nil
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
