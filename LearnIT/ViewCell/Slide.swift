//
//  Slide.swift
//  LearnIT
//
//  Created by iosdev on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class Slide: UIView{
    // Generated slides for onboarding page
    @IBAction func buttonClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "tabBarController")
        self.window?.rootViewController = vc
    }
    
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var onboardingDescription: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var onboardingLabel: UILabel!
}
