//
//  CourseLevelTableViewCell.swift
//  LearnIT
//
//  Created by Thath on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class CourseLevelTableViewCell: UITableViewCell {
    
    @IBOutlet var collectionViewOutlet: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // reload data only in coullectionView.
    func reloadData() {
        collectionViewOutlet.reloadData()
    }
}
