//
//  LangCollectionViewCell.swift
//  LangMaster
//
//  Created by Thath on 23/04/2019.
//  Copyright © 2019 Thath. All rights reserved.
//

import UIKit

// create a class for LangCollectionView cell
class LangCollectionViewCell: UICollectionViewCell {
    @IBOutlet var langLogoImageView: UIImageView!
}
