//
//  BookmarkViewController.swift
//  LearnIT
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

class BookmarkViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{
    @IBOutlet weak var bookmarkTable: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    var placeholderArray = [1,2,3,4,5]
    func numberOfSections(in tableView: UITableView) -> Int {
        return placeholderArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookmarkCell", for: indexPath)
        cell.textLabel?.text = String(placeholderArray[indexPath.section])
        cell.backgroundColor = cellBackgroundColor(cellIndex: indexPath.section, numberOfElements: placeholderArray.count)
        cell.layer.cornerRadius = 8
        return cell
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = placeholderArray[sourceIndexPath.section]
        placeholderArray.remove(at: sourceIndexPath.section)
        placeholderArray.insert(item, at: destinationIndexPath.section)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete) {
            placeholderArray.remove(at: indexPath.section)
            tableView.deleteSections([indexPath.section], with: .automatic)
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func cellBackgroundColor(cellIndex: Int, numberOfElements: Int) -> UIColor {
        switch cellIndex + 1 {
        case 0...numberOfElements/3:
            return UIColor.orange
        case numberOfElements/3...2*numberOfElements/3:
            return UIColor.yellow
        default:
            return UIColor.green
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5 as CGFloat
    }
    
    @IBAction func editButton(_ sender: Any) {
        bookmarkTable.isEditing = !bookmarkTable.isEditing
        switch bookmarkTable.isEditing {
        case true:
            editButton.title = "Done"
        case false:
            editButton.title = "Edit"
        }
    }
    
    //Switches tabs after swipe
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .left {
            if (self.tabBarController?.selectedIndex)! < 2
            { // set here  your total tabs
                self.tabBarController?.selectedIndex += 1
            }
        } else if gesture.direction == .right {
            if (self.tabBarController?.selectedIndex)! > 0 {
                self.tabBarController?.selectedIndex -= 1
            }
        }
    }
    
    override func viewDidLoad() {
        //Swipe gestures for swiping between tabs
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped(_:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        swipeLeft.delegate = self
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        swipeLeft.delegate = self
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
    }
    
    
}
