//
//  ViewController.swift
//  LearnIT
//
//  Created by Thath on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate  {
    
    // UI outlets
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    // Array of courses for each level
    var basicCourseArr = [Courses]()
    var advancedCourseArr = [Courses]()
    var frameworkCourseArr = [Courses]()
    
    // Updates array of courses when user is searching
    var isSearching: Bool = false
    var searchBasicCourseArr = [Courses]()
    var searchAdvancedCourseArr = [Courses]()
    var searchFrameWorkCourseArr = [Courses]()
    
    // Instance of data fetcher
    var dataFetcher = DataFetcher()

    var selectedCourse:Courses!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Sets bookmark badge count to the number of elements from core data
        self.tabBarController?.tabBar.items?[1].badgeValue = String(FetchCoreData().fetchbookMarkId().count)

        // retrieve all user skills from UserDefaults
        let skills:[String] = (UserDefaults.standard.stringArray(forKey: "skills") ?? [String]()).map{$0.lowercased()}
        
        // fetch data from core data with a filter to remove courses related to known skills
        DispatchQueue.global(qos: .userInteractive).async {
            let dataFetcher = DataFetcher()
            dataFetcher.getJSONData(completionHandler: {jsonData, error in
                if let jsonData = jsonData {
                    let tabBar = self.tabBarController as! TabBarController
                    self.courseDatabase = jsonData
                    if tabBar.searchingKeyword == "" {
                        self.basicCourseArr = self.courseDatabase.courseLevels.basicCourses.filter{!skills.contains($0.no.lowercased())}
                        self.advancedCourseArr = self.courseDatabase.courseLevels.advancedCourses.filter{!skills.contains($0.no.lowercased())}
                        self.frameworkCourseArr = self.courseDatabase.courseLevels.frameworkCourses.filter{!skills.contains($0.name.lowercased())}
                        DispatchQueue.main.async {
                            let tabBar = self.tabBarController as! TabBarController
                            self.searchBar.text = tabBar.searchingKeyword
                            self.searchBar(self.searchBar, textDidChange: tabBar.searchingKeyword)
                            self.tableView.reloadData()
                        }
                    }   else {
                            self.basicCourseArr = self.courseDatabase.courseLevels.basicCourses
                            self.advancedCourseArr = self.courseDatabase.courseLevels.advancedCourses
                            self.frameworkCourseArr = self.courseDatabase.courseLevels.frameworkCourses
                            DispatchQueue.main.async {
                                let tabBar = self.tabBarController as! TabBarController
                                self.searchBar.text = tabBar.searchingKeyword
                                self.searchBar(self.searchBar, textDidChange: tabBar.searchingKeyword)
                                self.tableView.reloadData()
                            }
                        }
                }
            })
        }
        
        // hide navigation bar because this is the root View controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the Navigation Bar so that we will have it on the next View
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = NSLocalizedString("Search for language or framework", comment: "Search for language or framework")
        self.hideKeyboardWhenTappedAround()          // hide the keyboard when tapping around
        self.tabBarController?.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.7422073287, green: 0.4305739439, blue: 0.009549473904, alpha: 1)
        
        // fetch data from json serve and assign to array variables in order to show up on Collection View
        DispatchQueue.global(qos: .userInteractive).async {
            self.dataFetcher.getJSONData(completionHandler: {
                db, error in
                if let db = db {
                    self.courseDatabase = db
                    self.basicCourseArr = db.courseLevels.basicCourses
                    self.advancedCourseArr = db.courseLevels.advancedCourses
                    self.frameworkCourseArr = db.courseLevels.frameworkCourses
                    DispatchQueue.main.async {
                        let tabBar = self.tabBarController as! TabBarController
                        self.searchBar.text = tabBar.searchingKeyword
                        self.searchBar(self.searchBar, textDidChange: tabBar.searchingKeyword)
                        self.tableView.reloadData()
                    }
                }
            })
        }
        searchBar.barTintColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)   // set up color for search bar
        self.tabBarController?.tabBar.tintColor = UIColor.white      // set text color to tabbar
        self.tabBarController?.tabBar.barTintColor = UIColor.orange     // set bar color
        
        // apply swipe gesture
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped(_:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        swipeLeft.delegate = self
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    // set up swipe gesture
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .left {
            if (self.tabBarController?.selectedIndex)! < 2 {
                self.tabBarController?.selectedIndex += 1
            }
        } else if gesture.direction == .right {
            if (self.tabBarController?.selectedIndex)! > 0 {
                self.tabBarController?.selectedIndex -= 1
            }
        }
    }
    
    // this function called when user input text in search bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // if the search keyword is empty
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            tableView.reloadData()
        } else {
            // if there is some keyword, so fetch data to search variable to show seaching result.
            isSearching = true
            searchBasicCourseArr = basicCourseArr.filter { $0.title.lowercased().contains((searchText.lowercased())) || $0.no.lowercased().contains((searchText.lowercased())) || $0.name.lowercased().contains((searchText.lowercased())) }
            searchAdvancedCourseArr = advancedCourseArr.filter { $0.title.lowercased().contains((searchText.lowercased())) || $0.no.lowercased().contains((searchText.lowercased())) || $0.name.lowercased().contains((searchText.lowercased())) }
            searchFrameWorkCourseArr = frameworkCourseArr.filter { $0.title.lowercased().contains((searchText.lowercased())) || $0.no.lowercased().contains((searchText.lowercased())) || $0.name.lowercased().contains((searchText.lowercased())) }
            self.tableView.reloadData()
        }
    }
    
    //hide the Keyboard when scrolling or clicking on Search Button
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    var courseDatabase:Database! // contain all courses
    // if no searching result so the row will be narrowed/closed
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isSearching && searchBasicCourseArr.count == 0 {
                return 0
            } else {
                return 1
            }
        case 1:
            if isSearching && searchAdvancedCourseArr.count == 0 {
                return 0
            } else {
                return 1
            }
        default:
            if isSearching && searchFrameWorkCourseArr.count == 0 {
                return 0
            } else {
                return 1
            }
        }
    }
    
    // set the number of sections for table view, we have 3 sections of basic courses, advanced courses and framework courses
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    // create a cell/row for table View
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellY", for: indexPath) as? CourseLevelTableViewCell else {return UITableViewCell()}
        cell.collectionViewOutlet.tag = indexPath.section    // assign index of section to tag in order to realize the section where the collection items are.
        cell.reloadData()
        return cell
    }

    // make header for section.
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 30))
        returnedView.backgroundColor = .lightGray
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 30))
        switch section {
        case 0:
            label.text = NSLocalizedString("     Basic Courses", comment: "Basic Courses")
        case 1:
            label.text = NSLocalizedString("     Advanced Courses", comment: "Advanced Courses")
        default:
            label.text = NSLocalizedString("     Framework Courses", comment: "Framework Courses")
        }
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        label.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
        label.textAlignment = .left;
        returnedView.addSubview(label)
        return returnedView
    }
}

// create a collection view nested with table view
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // set up number of items in a section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching {
            if collectionView.tag == 0 {
                return self.searchBasicCourseArr.count
            } else if collectionView.tag == 1 {
                return self.searchAdvancedCourseArr.count
            } else {
                return self.searchFrameWorkCourseArr.count
            }
        } else {
            if collectionView.tag == 0 {
                return self.basicCourseArr.count
            } else if collectionView.tag == 1 {
                return self.advancedCourseArr.count
            } else {
                return self.frameworkCourseArr.count
            }
        }
    }
    
    // design and fetch data to collectione view cell
    private func setupCell(cell: CourseCollectionViewCell, courseArr: [Courses], index: Int){
        let url = NSURL(string: courseArr[index].linkToImage.replacingOccurrences(of: "https://i.udemycdn.com/course/480x270/", with: "https://i.udemycdn.com/course/240x135/"))
        if let data = NSData(contentsOf: url! as URL) {
            cell.imageView.contentMode = UIView.ContentMode.scaleAspectFit
            cell.imageView.image = UIImage(data: data as Data)
            cell.titleLable.text = courseArr[index].title
            cell.priceLable.text = "€ "+courseArr[index].price
            cell.ratingView.rating = Double(courseArr[index].ratings) ?? 1.0
            cell.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        }
    }
    
    // set up cell in collection view inside table row
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellX", for: indexPath) as! CourseCollectionViewCell
        if isSearching {
            if collectionView.tag == 0 {
                if !searchBasicCourseArr.isEmpty {
                    setupCell(cell: cell, courseArr: searchBasicCourseArr, index: indexPath.item)
                }
            } else if collectionView.tag == 1 {
                if !searchAdvancedCourseArr.isEmpty {
                    setupCell(cell: cell, courseArr: searchAdvancedCourseArr, index: indexPath.item)
                }
            } else {
                if !searchFrameWorkCourseArr.isEmpty {
                    setupCell(cell: cell, courseArr: searchFrameWorkCourseArr, index: indexPath.item)
                }
            }
        } else {
            if collectionView.tag == 0 {
                if !basicCourseArr.isEmpty {
                    setupCell(cell: cell, courseArr: basicCourseArr, index: indexPath.item)
                }
            } else if collectionView.tag == 1 {
                if !advancedCourseArr.isEmpty {
                    setupCell(cell: cell, courseArr: advancedCourseArr, index: indexPath.item)
                }
            } else {
                if !frameworkCourseArr.isEmpty {
                   setupCell(cell: cell, courseArr: frameworkCourseArr, index: indexPath.item)
                }
            }
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // assign selected course to selectedCourse in order to pass to new CourseBrief View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if isSearching {
            if collectionView.tag == 0 {
                selectedCourse = searchBasicCourseArr[indexPath.item]
            } else if collectionView.tag == 1 {
                selectedCourse = searchAdvancedCourseArr[indexPath.item]
            } else {
                selectedCourse = searchFrameWorkCourseArr[indexPath.item]
            }
        } else {
            if collectionView.tag == 0 {
                selectedCourse = basicCourseArr[indexPath.item]
            } else if collectionView.tag == 1 {
                selectedCourse = advancedCourseArr[indexPath.item]
            } else {
                selectedCourse = frameworkCourseArr[indexPath.item]
            }
        }
        performSegue(withIdentifier: "homeToBriefV", sender: self)
    }
    
    // create a segue for navigation to CourseBrief View
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc = segue.destination as? CourseBriefViewController
        svc?.courseBrief = selectedCourse
    }
    
    // set back empty to search keyword that help no longer keep the searching result after view disapears
    override func viewDidDisappear(_ animated: Bool) {
        let tabBar = self.tabBarController as! TabBarController
        tabBar.searchingKeyword = ""
    }
}


extension UIViewController {
    // hide keyboard by tapping around
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
