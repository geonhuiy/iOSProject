//
//  CourseBriefViewController.swift
//  LangMaster
//
//  Created by Thath on 25/04/2019.
//  Copyright © 2019 Thath. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData

class CourseBriefViewController: UIViewController {
    
    var courseBrief: Courses!
    var currentTabBarIndex: Int!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var whatYouLearn: UILabel!
    @IBOutlet weak var courseDesc: UILabel!
    @IBOutlet weak var webButton: UIButton!
    
    // this function will use to bring user to Webpage of the selected course
    @IBAction func goToWeb(_ sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "BriefToWeb", sender: "")
        }
    }
    
    // create a segue and pass course's info to WebPage View
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc = segue.destination as? WebPageViewController
        svc?.courseBrief = courseBrief
    }
    
    // 
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // this even will be triggered when the Bookmark Button clicked
    @objc func tapBookMarkButton(){
        //check if the course already exists in Boommarks
        if !(FetchCoreData().fetchbookMarkId()).contains(courseBrief.id) {
            //store the course id to core data
            let bookMark = BookmarkCourseArray(context: PersistenceService.context)
            bookMark.bookmarkCourseId = Int16(courseBrief.id)
            PersistenceService.saveContext()
            // pop up a successful notification
            self.successfulNotification()
        } else {
            // call an alert, that this course is already in bookmarks
            self.bookmarkAlert()
        }
    }
    
    func clickedBookmarkButton(alert: UIAlertAction) {
        self.tabBarController?.selectedIndex = 1
    }
    // Alert the user after adding a course to bookmarks
    func bookmarkAlert() {
        let alert = UIAlertController(title: "Notification", message: NSLocalizedString("This course is already added in your bookmarks", comment: "This course is already added in your bookmarks"), preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        let bookmarkButton = UIAlertAction(title: "Bookmarks", style: .cancel, handler: self.clickedBookmarkButton)
        alert.addAction(okButton)
        alert.addAction(bookmarkButton)
        self.present(alert, animated: true, completion: nil)
    }

    func successfulNotification() {
        //Set up a notification
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = courseBrief.title
        content.subtitle = courseBrief.subtitle
        content.body = NSLocalizedString("Added this course into your bookmarks", comment: "Added this course into your bookmarks")
        content.sound = UNNotificationSound.default
        content.threadIdentifier = "thread"
        let date = Date(timeIntervalSinceNow: 1)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let request = UNNotificationRequest(identifier: "content", content: content, trigger: trigger)
        center.add(request) {(error) in
            if error != nil {
                print(error!)
            }
        }
        // update number of bookmark courses on boomark badge
        self.tabBarController?.tabBar.items?[1].badgeValue = String(FetchCoreData().fetchbookMarkId().count)
    }
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet var enrolledLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var publishTimeLable: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var originPriceLabel: UILabel!
    
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var whatwilllearnTitleLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        //update the bookmark badge
        self.tabBarController?.tabBar.items?[1].badgeValue = String(FetchCoreData().fetchbookMarkId().count)
    }
    override func viewDidLoad() {
        
        // get the current tab bar index, we will use it to recognize if naviagtion to the view on the same tab or not
        currentTabBarIndex = self.tabBarController?.selectedIndex
        
        UINavigationBar.appearance().tintColor = UIColor.white
        let url = NSURL(string: courseBrief?.linkToImage ?? "")
        if let data = NSData(contentsOf: url! as URL) {
            imageView.image = UIImage(data: data as Data)
        }
        
        // show course info on the UI views
        self.title = courseBrief?.name.capitalized
        webButton.setTitle(NSLocalizedString("More Details", comment: "More Details"), for: .normal)
        descriptionTitleLabel.text = NSLocalizedString("Description:", comment: "Description of course")
        whatwilllearnTitleLabel.text = NSLocalizedString("What Will You Learn:", comment: "What can be learned on this course")
        titleLabel.text = courseBrief.title
        titleLabel.font = UIFont.boldSystemFont(ofSize: 28.0)
        subtitleLabel.text = courseBrief?.subtitle ?? ""
        subtitleLabel.font = UIFont.boldSystemFont(ofSize: 24.0)
        ratingLabel.setTextWithIconImage(image: UIImage(named: "Ratings")!, with: courseBrief.ratings)
        starLabel.setTextWithIconImage(image: UIImage(named: "Star")!, with: courseBrief.stars)
        priceLabel.setTextWithIconImage(image: UIImage(named: "Price")!, with: "€"+courseBrief.price)
        enrolledLabel.setTextWithIconImage(image: UIImage(named: "EnrolledN")!, with: courseBrief.studentN)
        hourLabel.setTextWithIconImage(image: UIImage(named: "StudyTime")!, with: courseBrief.studyHours)
        publishTimeLable.setTextWithIconImage(image: UIImage(named: "UpdateTime")!, with: courseBrief.updateTime)
        authorLabel.setTextWithIconImage(image: UIImage(named: "Author")!, with: NSLocalizedString("Created by ", comment: "Created by ")+courseBrief.author)
        originPriceLabel.setTextWithIconImage(image: UIImage(named: "Price")!, with: "€"+courseBrief.originPrice)
        whatYouLearn.text = courseBrief?.whatToLearn
        courseDesc.text = courseBrief?.description
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: " €"+courseBrief.originPrice+" ")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        originPriceLabel.attributedText = attributeString
        UIFont.boldSystemFont(ofSize: 16.0)
        
        // Create add bookmark button on Navigation Bar
        let bookmarkButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(self.tapBookMarkButton))
        self.navigationItem.rightBarButtonItem = bookmarkButton
    }

    // dismiss all view to the root view before going to another view on another tab
    override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != currentTabBarIndex {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
extension UILabel {
        // set up label with text and icon inside border
    func setTextWithIconImage(image: UIImage, with text: String) {
        let text2 = " " + text + "    "
        let attachment = NSTextAttachment()
        attachment.image = image
        let imageOffsetY:CGFloat = -2.0;
        attachment.bounds = CGRect(x: 0, y: imageOffsetY, width: image.size.width - 8, height: image.size.height - 8)
        let attachmentStr = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentStr)
        let textString = NSAttributedString(string: text2, attributes: [.font: self.font])
        mutableAttributedString.append(textString)
        self.attributedText = mutableAttributedString
        self.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.textAlignment = .center
    }
}
