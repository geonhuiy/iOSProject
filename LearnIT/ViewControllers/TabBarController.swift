//
//  TabBarController.swift
//  LearnIT
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

// create a controller for tabbar
class TabBarController: UITabBarController {
    var searchingKeyword: String = ""
    override func viewDidLoad() {
        delegate = self
    }
}

extension TabBarController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TransitionObject()
    }
}
