//
//  WebPageViewController.swift
//  LearnIT
//
//  Created by Thath on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import WebKit
import UserNotifications
import CoreData

class WebPageViewController: UIViewController, WKNavigationDelegate {
    var courseBrief: Courses!
    var currentTabBarIndex: Int!    // store current tab bar index
    var bookmarkIdArray = FetchCoreData().fetchbookMarkId();

    @IBOutlet var web: WKWebView!
    @objc func tapBookMarkButton(){
        //store the course id to core data
        let bookMark = BookmarkCourseArray(context: PersistenceService.context)
        bookMark.bookmarkCourseId = Int16(courseBrief.id)
        PersistenceService.saveContext()
        self.successfulNotification()
        self.tabBarController?.tabBar.items?[1].badgeValue = String(FetchCoreData().fetchbookMarkId().count)
    }
    
    func successfulNotification() {
        
        //Set up a notification
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = courseBrief.title
        content.subtitle = courseBrief.subtitle
        content.body = "Added this course into your bookmarks"
        content.sound = UNNotificationSound.default
        content.threadIdentifier = "thread"
        let date = Date(timeIntervalSinceNow: 1)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let request = UNNotificationRequest(identifier: "content", content: content, trigger: trigger)
        center.add(request) {(error) in
            if error != nil {
                print(error!)
            }
        }
    }
    
    // pop the current view to root View before going to another view on another tabbar
    override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != currentTabBarIndex {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentTabBarIndex = self.tabBarController?.selectedIndex  // assign the tabbar index
        //let fetch = FetchCoreData();
        DispatchQueue.global().async {
            if self.courseBrief.linkToWeb != "" {
                let request = URLRequest(url: URL(string: self.courseBrief.linkToWeb)!)
                DispatchQueue.main.async {
                    self.web.load(request)
                    self.web.allowsBackForwardNavigationGestures = true
                }
            }
        }
        
        //create add Button on Navigation Bar, and hide it if the courses already existed
        var onBookMarks: Bool = false
        for item in bookmarkIdArray {
            if(courseBrief.id == item){
                onBookMarks = true
            }
        }
        if(!onBookMarks){
            let bookmarkButton = UIBarButtonItem(
                barButtonSystemItem: .bookmarks,
                target: self,
                action: #selector(tapBookMarkButton))
            self.navigationItem.rightBarButtonItem = bookmarkButton
        }
    }
}

