//
//  ViewController3.swift
//  LangMaster
//
//  Created by Thath on 23/04/2019.
//  Copyright © 2019 Thath. All rights reserved.
//

import UIKit

// create this class to control the ProfileView
class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate {
    
    // set up how many items in a section in the collectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return languageArr.count
    }
    
    // design and fetch data to cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "langCell", for: indexPath) as! LangCollectionViewCell
        cell.langLogoImageView.image = UIImage(named: languageArr[indexPath.item].logo)
        cell.langLogoImageView.setImageColor(color: .white)
        cell.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
        cell.layer.cornerRadius = 60 //set corner radius here
        cell.layer.shadowRadius = 10
        cell.layer.masksToBounds = false
        cell.layer.shadowOpacity = 1
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.layer.shadowColor = UIColor.orange.cgColor
        cell.alpha = 1
        return cell
    }
    
    // this function to handel the even when the item clicked on
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedLanguage = languageArr[indexPath.row]
        performSegue(withIdentifier: "ProfileToChecklistSkill", sender: "")
    }
    
    // create a segue for navigation to SkillView and pass value to SkillView language variable
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let svc = segue.destination as? SkillViewController
        svc?.language = selectedLanguage
    }
    
    var selectedLanguage: Language!  // contain language'info selected

    @IBOutlet var langCollectionView: UICollectionView!
    
    var languageArr: [Language] = []  // contain language'info in array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Language Portfolio", comment: "Language Portfolio")    // set title to the View
        
        // set up Swipe Gesture
        view.accessibilityIdentifier = "portfolioView"
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        swipeRight.delegate = self
        self.view.addGestureRecognizer(swipeRight)
        
        // set up text color on navigation bar
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        // design the collection View layout padding
        let layout = self.langCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 10,left: 10,bottom: 0,right: 10)
        
        // load json data
        DispatchQueue.global(qos: .userInteractive).async {
            let dataFetcher = DataFetcher()
            dataFetcher.getJSONData(completionHandler: {jsonData, error in
                if let jsonData = jsonData {
                    self.languageArr = jsonData.languages
                    DispatchQueue.main.async {
                        self.langCollectionView.reloadData()
                    }
                }
            })
        }
    }
    
    // Swiping between tabs
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .left {
            if (self.tabBarController?.selectedIndex)! < 3
            {
                self.tabBarController?.selectedIndex += 1
            }
        } else if gesture.direction == .right {
            if (self.tabBarController?.selectedIndex)! > 0 {
                self.tabBarController?.selectedIndex -= 1
            }
        }
    }
}

extension UIImageView {
    // set up image color
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
