//
//  StudyPathViewController.swift
//  LangMaster
//
//  Created by Thath on 01/05/2019.
//  Copyright © 2019 Thath. All rights reserved.
//

import UIKit

class SkillViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ProfileCellSubclassDelegate {
 
    @IBOutlet var tableView: UITableView!
    var language: Language?
    var currentTabBarIndex: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        currentTabBarIndex = self.tabBarController?.selectedIndex   // store current tab bar index
        
        self.title = NSLocalizedString("Language Portfolio", comment: "Header of view") // set header text for View
        
        // set no breakline between items
        self.tableView.separatorStyle = .none
        
        // add 2 strings: "Basic" and "Advanced" to array to show on table row
        language?.frameworks.insert(contentsOf: [NSLocalizedString("Basic", comment: "Course level: Basic"),NSLocalizedString("Advanced", comment: "Course level: Advanced")], at: 0)
    }
    
    // set up the number of rows in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return language?.frameworks.count ?? 1
    }
    
    // set up the cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "checklistSkill", for: indexPath) as! CheckSkillTableViewCell

        // design a cell
        cell.languageLevelLabel.text = language!.frameworks[indexPath.row]
        cell.languageLevelLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        cell.languageLevelLabel.textColor = .white
        
        // fetch skill array from UserDefaults
        let skills:[String] = UserDefaults.standard.stringArray(forKey: "skills") ?? [String]()

        // set up checkbox where user know the skills and color of cell background
        switch language?.frameworks[indexPath.row].lowercased() {
        case "basic":
            if skills.contains(language!.name + "-1"){
                cell.checkBoxLabel.isSelected = true
            }
            cell.courseView.layer.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        case "advanced":
            if skills.contains(language!.name + "-2"){
                cell.checkBoxLabel.isSelected = true
            }
            cell.courseView.layer.backgroundColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        default:
            if skills.contains(language!.frameworks[indexPath.row]){
                cell.checkBoxLabel.isSelected = true
            }
            cell.courseView.layer.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        }
        
        // create the cell border
        cell.courseView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        cell.courseView.layer.borderWidth = 0.5
        cell.courseView.layer.cornerRadius = 12
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }

    // this function invoked when the search button clicked
    func searchButtonTapped(cell: CheckSkillTableViewCell, sender: UIButton) {
        
        // get the index of cell
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        
        // create a tabbar to store the seaching keyword
        let tabBar = tabBarController as! TabBarController
        switch language?.frameworks[indexPath.row].lowercased() {
        case "basic":
            tabBar.searchingKeyword = language!.name + "-1"
        case "advanced":
            tabBar.searchingKeyword = language!.name + "-2"
        default:
            tabBar.searchingKeyword = language!.frameworks[indexPath.row].lowercased()
        }
        // navigate to the SeachView
        self.tabBarController?.selectedIndex = 0
    }
    
    // this function invoked when the checkbox clicked
    func checkBoxTapped(cell: CheckSkillTableViewCell, sender: UIButton) {
        
        // get the index of cell
        let indexPath = self.tableView.indexPath(for: cell)
        guard self.tableView.indexPath(for: cell) != nil else { return }
        
        // set up checkbox click animation
        UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCurlDown, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            sender.isSelected = !sender.isSelected
        }) { (success) in
            UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCurlDown, animations: {
                sender.transform = .identity
            }, completion: nil)
            
        }
        
        // retriece the skills fron UserDefaults to restore the skills again after updating
        let skills:[String] = UserDefaults.standard.stringArray(forKey: "skills") ?? [String]()
        if sender.isSelected {
            switch self.language?.frameworks[indexPath!.row].lowercased() {
            case "basic":
                UserDefaults.standard.set(skills + [self.language!.name + "-1"], forKey: "skills")
            case "advanced":
                UserDefaults.standard.set(skills + [self.language!.name + "-2"], forKey: "skills")
            default:
                UserDefaults.standard.set(skills + [self.language!.frameworks[indexPath!.row]], forKey: "skills")
            }
        } else {
            switch self.language?.frameworks[indexPath!.row].lowercased() {
            case "basic":
                UserDefaults.standard.set(skills.filter({$0 != self.language!.name + "-1"}), forKey: "skills")
            case "advanced":
                UserDefaults.standard.set(skills.filter({$0 != self.language!.name + "-2"}), forKey: "skills")
            default:
                UserDefaults.standard.set(skills.filter({$0 != self.language!.frameworks[indexPath!.row]}), forKey: "skills")
            }
        }
    }
    
    // get the the root view after go to new view on another tab
    override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != currentTabBarIndex {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
