//
//  OnboardingViewController.swift
//  LearnIT
//
//  Created by iosdev on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit
class OnboardingViewController: UIViewController, UIScrollViewDelegate{
    var slides: [Slide] = []
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        scrollView.delegate = self
        super.viewDidLoad()
        view.accessibilityIdentifier=("onboardingView")
        slides = createOnboardingSlides()
        
        // Sets border style for all imageviews in slide
        for i in slides {
            i.imageView.layer.borderColor = UIColor.orange.cgColor
            i.imageView.layer.cornerRadius = 50
            i.imageView.layer.borderWidth = 2
        }
        
        setupSlideScrollView(slides: slides)
        
        // Number of pages from slides count
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        
        view.bringSubviewToFront(pageControl)
    }
    
    // Individual onboarding slide design
    func createOnboardingSlides() -> [Slide] {
        let slide1: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.imageView.image = UIImage(named: "langmaster_logo")
        slide1.onboardingLabel.text = "Langmaster"
        slide1.onboardingDescription.text = NSLocalizedString("Swipe left to continue", comment: "Swipe left to continue")
        // Get started button is hidden until last slide
        slide1.getStartedButton.isHidden = true
        
        let slide2: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.imageView.image = UIImage(named: "slide2.png")
        slide2.onboardingLabel.text = NSLocalizedString("Search", comment: "Search")
        slide2.onboardingDescription.text = NSLocalizedString("Find desired online courses with ease", comment: "Find desired online courses with ease")
        slide2.getStartedButton.isHidden = true
        
        let slide3: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.imageView.image = UIImage(named: "slide3.png")
        slide3.onboardingLabel.text = NSLocalizedString("Save", comment: "Save")
        slide3.onboardingDescription.text = NSLocalizedString("Found a course that you like? Save it into bookmarks for later viewing", comment: "Found a course that you like? Save it into bookmarks for later viewing")
        slide3.getStartedButton.isHidden = true
    
        let slide4: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide4.getStartedButton.setTitle(NSLocalizedString("Get started", comment: "Get stated"), for: .normal)
        slide4.imageView.image = UIImage(named: "slide4.png")
        slide4.onboardingLabel.text = NSLocalizedString("Manage", comment: "Manage")
        slide4.onboardingDescription.text = NSLocalizedString("Keep yourself up to date with your progress", comment: "Keep yourself up to date with your progress")
        return [slide1,slide2,slide3,slide4]
    }
    
    func setupSlideScrollView(slides : [Slide]) {
        // Slide transition
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Changes pages on page control
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
    }
    
    // Recreates slides if device orientation changes
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setupSlideScrollView(slides: slides)
    }
}
