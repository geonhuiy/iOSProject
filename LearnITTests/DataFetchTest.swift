//
//  DataFetchTest.swift
//  LearnITTests
//
//  Created by iosdev on 08/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import LearnIT
class DataFetchTest: XCTestCase {
    let testFetcher = DataFetcher()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testFetch() {
        testFetcher.getJSONData(completionHandler: {
            db, error in
            if let db = db {
                XCTAssertFalse(db.courseLevels.advancedCourses.isEmpty)
                XCTAssertFalse(db.courseLevels.basicCourses.isEmpty)
                XCTAssertFalse(db.courseLevels.frameworkCourses.isEmpty)
            }
        })
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
