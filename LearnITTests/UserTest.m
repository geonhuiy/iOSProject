//
//  UserTest.m
//  LearnITTests
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "User.h"

@interface UserTest : XCTestCase
@property User *testUser;
@end

@implementation UserTest

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    [super setUp];
    _testUser = [[User alloc]init];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testSetExistingSkillSet {
    NSSet *testSet = [NSSet setWithArray:@[@1,@2,@3]];
    [_testUser setExistingSkillSet:testSet];
    XCTAssert([_testUser skillSet] == testSet);
}

- (void)testSetBookmarkArray {
    NSArray *testArray = @[@1,@2,@3];
    [_testUser setBookmarkArray:testArray];
    XCTAssert([_testUser bookmarkArray] == testArray);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
