//
//  LearnITUITests.swift
//  LearnITUITests
//
//  Created by iosdev on 16/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest

class LearnITUITests: XCTestCase {
    var app : XCUIApplication!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        app = XCUIApplication()
        app.launchArguments.append("--testing")
    }
    
    func testOnboarding() {
        app.launch()
        XCTAssert(app.isOnboarding)
        app.swipeLeft()
        app.swipeLeft()
        app.swipeLeft()
        app.buttons["getStartedButton"].tap()
        XCTAssertFalse(app.isOnboarding)
    }

    func testProfilePage(){
        app.launch()
        testOnboarding()
        app.tabBars.buttons["More"].tap()
        
        let cellsQuery = app.collectionViews.cells
        print(cellsQuery.count)
        
        let rubyElement = cellsQuery.otherElements.containing(.image, identifier:"Ruby").element
        rubyElement.tap()
        
        let tablesQuery = app.tables
        print("cells \(tablesQuery.cells.count)")
         XCTAssertTrue(tablesQuery.cells.count == 3)
        let basic = tablesQuery.cells.containing(.staticText, identifier:"Basic").buttons["Button"]
         XCTAssertTrue(basic.exists)
        basic.tap()
        
        let advanced = tablesQuery.cells.containing(.staticText, identifier:"Advanced").buttons["Button"]
         XCTAssertTrue(advanced.exists)
        advanced.tap()
        
        var framwork = tablesQuery.cells.containing(.staticText, identifier:"Rails").buttons["Button"]
         XCTAssertTrue(framwork.exists)
        framwork.tap()
        let languagePortfolioButton = app.navigationBars["Language Portfolio"].buttons["Language Portfolio"]
         XCTAssertTrue(languagePortfolioButton.exists)
        languagePortfolioButton.tap()
        cellsQuery.otherElements.containing(.image, identifier:"JavaScript").element.tap()
        languagePortfolioButton.tap()
    }
    
    func testNavTabs() {
        let app = XCUIApplication()
        app.launch()
        
        let tabBarsQuery = XCUIApplication().tabBars
        XCTAssert(tabBarsQuery.buttons["Bookmarks"].exists)
        XCTAssert(tabBarsQuery.buttons["More"].exists)
        XCTAssert(tabBarsQuery.buttons["Search"].exists)
        
        tabBarsQuery.buttons["Bookmarks"].tap()
        XCTAssert(app.isBookmarkPage)
        tabBarsQuery.buttons["More"].tap()
        XCTAssert(app.isProfilePage)
        tabBarsQuery.buttons["Search"].tap()
        XCTAssert(app.isSearchPage)
    }
    /*func testCourseViews(){
        
        let app = XCUIApplication()
        app.launch()
        XCTAssertTrue(app.exists)
        let backButton = app.navigationBars["Python"].buttons["Back"]
        backButton.tap()
        
        let courseView = app.tables/*@START_MENU_TOKEN@*/.staticTexts["Python for Beginners"]/*[[".cells.staticTexts[\"Python for Beginners\"]",".staticTexts[\"Python for Beginners\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(courseView.exists)
        courseView.tap()
        
        let elementsQuery = app.scrollViews.otherElements
        elementsQuery.staticTexts["How to create the entire campaign from beginning to end. Be able to build any app you want. Start your own app based business. Master app design so you'll know how to wireframe, mockup and prototype your app idea. All knowledge, tools and skills to find a job"].swipeUp()
        elementsQuery.buttons["More Details"].tap()
        
        let webViewsQuery = app.webViews
        webViewsQuery/*@START_MENU_TOKEN@*/.staticTexts["to give you the best online experience. By using our website, you agree to our use of cookies in accordance with our cookie policy."]/*[[".otherElements[\"Learn Python: Python for Beginners | Udemy\"].staticTexts[\"to give you the best online experience. By using our website, you agree to our use of cookies in accordance with our cookie policy.\"]",".staticTexts[\"to give you the best online experience. By using our website, you agree to our use of cookies in accordance with our cookie policy.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        webViewsQuery/*@START_MENU_TOKEN@*/.staticTexts[""]/*[[".otherElements[\"Learn Python: Python for Beginners | Udemy\"].staticTexts[\"\"]",".staticTexts[\"\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        let buttonPython = app.navigationBars["LearnIT.WebPageView"].buttons["Python"]
        XCTAssertTrue(buttonPython.exists)
        buttonPython.tap()
        
        XCTAssertTrue(courseView.exists)
        backButton.tap()
    }*/

    /*func testSearch(){
        
        let validSearchWord = "vue"
        let app = XCUIApplication()
        XCTAssertTrue(app.exists)
        let searchForLanguagesOrFrameworksSearchField = app.searchFields["Search for languages or frameworks"]
        XCTAssertTrue(searchForLanguagesOrFrameworksSearchField.exists)
        searchForLanguagesOrFrameworksSearchField.tap();
        searchForLanguagesOrFrameworksSearchField.typeText(validSearchWord);
     
        let nextView = app.tables/*@START_MENU_TOKEN@*/.staticTexts["Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex)"]/*[[".cells.staticTexts[\"Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex)\"]",".staticTexts[\"Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex)\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
         XCTAssertTrue(nextView.exists)
        nextView.tap()
    }*/
}
