//
//  CurrentViewCheck.swift
//  LearnITUITests
//
//  Created by iosdev on 08/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit
import XCTest

extension XCUIApplication {
    var isOnboarding : Bool {
        return otherElements["onboardingView"].exists
    }
    
    var isSearchPage : Bool {
        return otherElements["searchView"].exists
    }
    
    var isBookmarkPage : Bool {
        return otherElements["bookmarkView"].exists
    }
    
    var isProfilePage : Bool {
        return otherElements["profileView"].exists
    }
}
